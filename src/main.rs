#![no_std]
#![no_main]
#![feature(lang_items)]

use atmega_hal::{
    clock, delay::Delay, pins, prelude::_embedded_hal_blocking_delay_DelayMs, Peripherals,
};
use atmega_hal::spi;
use core::panic::PanicInfo;

use ws2812_spi as ws2812;

use crate::ws2812::prerendered::Ws2812;
use smart_leds::{SmartLedsWrite, RGB8};

/**
what to do when your code program crashes
just looping forever is definitely not the most ideal way to handle this
but it works for now
and your code will never crash anyways :)
*/
#[panic_handler]
fn panic(_info: &PanicInfo) -> ! {
    loop {}
}


/**
it actually does not matter what this function is called
since we used the no_main macro, but this IS still our
main function
because of avr_device::entry
*/
#[avr_device::entry]
fn main() -> ! {
    let peripherals = Peripherals::take().unwrap();
    let pins = pins!(peripherals);
    let (spi, _) = spi::Spi::new(
        peripherals.SPI,
        pins.pb1.into_output(),
        pins.pb2.into_output(),
        pins.pb3.into_pull_up_input(),
        pins.pb0.into_output(),
        spi::Settings {
            clock: spi::SerialClockRate::OscfOver8,
            ..Default::default()
        }
    );
    let mut output_buffer = [0; 20 + (3 * 12)];
    let mut data: [RGB8; 3] = [RGB8::default(); 3];
    let empty: [RGB8; 3] = [RGB8::default(); 3];
    let mut ws = Ws2812::new(spi, &mut output_buffer);
    let mut delay = Delay::<clock::MHz16>::new();
    loop {
        data[0] = RGB8 {
            r: 0,
            g: 0,
            b: 0x10,
        };
        data[1] = RGB8 {
            r: 0,
            g: 0x10,
            b: 0,
        };
        data[2] = RGB8 {
            r: 0x10,
            g: 0,
            b: 0,
        };
        ws.write(data.iter().cloned()).unwrap();
        delay.delay_ms(1000 as u16);
        ws.write(empty.iter().cloned()).unwrap();
        delay.delay_ms(1000 as u16);
    }
}
